package com.ak.coyotetestapplication.domain

import androidx.paging.PagingData
import com.ak.coyotetestapplication.model.domain.User
import com.ak.coyotetestapplication.model.domain.UserDetails
import kotlinx.coroutines.flow.Flow

interface RandomUsersRepository {

    /**
     * Get random users from the api and database out of pagingData form
     *
     * @return
     */
    fun getRandomUsers(): Flow<PagingData<User>>

    /**
     * Get user details from database.
     *
     * @param userId is the id of the user fetched
     *
     * @return
     */
    suspend fun getUserDetails(userId: Long): UserDetails
}