package com.ak.coyotetestapplication.ui

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.ak.coyotetestapplication.ui.utils.UserDetailsDestination
import com.ak.coyotetestapplication.ui.utils.UsersListDestination

/**
 * This Composable represents the navigation graph in the application.
 */
@Composable
fun RandomUsersListNavHost(
    navController: NavHostController,
    modifier: Modifier = Modifier
) {
    NavHost(
        navController = navController,
        startDestination = UsersListDestination.route,
        modifier = modifier
    ) {
        composable(route = UsersListDestination.route) {
            UsersListScreen() { userId ->
                navController.navigateToUserDetails(
                    userId = userId
                )
            }
        }

        composable(
            route = UserDetailsDestination.routeWithArgs,
            arguments = UserDetailsDestination.arguments
        ){
            UserDetailsScreen()
        }
        }
    }

fun NavHostController.navigateToUserDetails(userId: Long) {
    this.navigate("${UserDetailsDestination.route}/$userId")
}