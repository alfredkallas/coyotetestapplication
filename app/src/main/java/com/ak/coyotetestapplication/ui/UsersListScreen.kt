package com.ak.coyotetestapplication.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.ak.coyotetestapplication.R
import com.ak.coyotetestapplication.model.domain.User
import com.ak.coyotetestapplication.model.ui.UiModel


/**
 * This composable represents the movies List Screen in the application
 */
@Composable
fun UsersListScreen(
    modifier: Modifier = Modifier,
    viewModel: UsersListViewModel = hiltViewModel(),
    onUserClick:(Long) -> Unit
) {

    val userListItems: LazyPagingItems<UiModel> = viewModel.pagingDataFlow.collectAsLazyPagingItems()
    // Implement composable here
    LazyColumn(
        contentPadding = PaddingValues(vertical = 16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        modifier = modifier
    ) {

        items(userListItems) { item ->
            when(item) {
                is UiModel.UserItem ->  UserCard(item.user) {
                    onUserClick(item.user.userId)
                }
                is UiModel.SeparatorItem -> UsersSeparatorCard(item.description)
                else -> Unit
            }
        }
        when (userListItems.loadState.append) {
                is LoadState.NotLoading -> Unit
                LoadState.Loading -> {
                item {
                    CircularProgressIndicator(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentWidth(Alignment.CenterHorizontally)
                    )
                }
            }
                is LoadState.Error -> {
                item {
                    ErrorItem(message = (userListItems.loadState.append as LoadState.Error).error.message.toString()) {
                        userListItems.retry()
                    }
                }
            }
        }

        if (userListItems.loadState.append == LoadState.Loading) {
            item {


                }
            }
        }
    }

@Composable
fun UsersSeparatorCard(title: String) {
    Surface(modifier = Modifier.background(MaterialTheme.colors.primaryVariant)) {
        Card(
            modifier = Modifier
                .padding(
                    bottom = 5.dp, top = 5.dp,
                    start = 5.dp, end = 5.dp
                )
                .fillMaxWidth(),
            shape = RoundedCornerShape(15.dp),
            elevation = 12.dp
        ) {
            Box(
                modifier = Modifier
                    .clip(RoundedCornerShape(4.dp))
                    .background(MaterialTheme.colors.surface),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = title,
                    fontWeight = FontWeight.Bold,
                    style = TextStyle(fontSize = 22.sp),
                    color = Color.Black
                )
            }
        }
    }
}


@Composable
fun UserCard(user: User, onClick: () -> Unit) {
    Card(
        modifier = Modifier
            .padding(
                bottom = 5.dp, top = 5.dp,
                start = 5.dp, end = 5.dp
            )
            .fillMaxWidth()
            .clickable(onClick = onClick),
        shape = RoundedCornerShape(15.dp),
        elevation = 12.dp
    ) {
        Row(
            modifier = Modifier
                .clip(RoundedCornerShape(4.dp))
                .background(MaterialTheme.colors.surface)
        ) {
            Surface(
                modifier = Modifier.size(48.dp),
                shape = RoundedCornerShape(12.dp),
                color = MaterialTheme.colors.surface.copy(
                    alpha = 0.2f)
            ) {
                val image = rememberAsyncImagePainter(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(user.thumbnail)
                        .crossfade(true)
                        .build(),
                    placeholder = painterResource(id = R.drawable.user),
                    contentScale = ContentScale.Crop)
                Image(
                    painter = image,
                    contentDescription = null,
                    modifier = Modifier
                        .height(100.dp)
                        .clip(shape = RoundedCornerShape(12.dp)),
                    contentScale = ContentScale.Crop
                )
            }
            Column(
                modifier = Modifier
                    .padding(start = 12.dp)
                    .align(Alignment.CenterVertically)
            ) {
                Text(
                    text = "${user.firstName} ${user.lastName}",
                    fontWeight = FontWeight.Bold,
                    style = TextStyle(fontSize = 22.sp),
                    color = Color.Black
                )
            }
        }
    }
}

@Composable
fun ErrorItem(message: String, onRetryClicked: () -> Unit) {
    Card(
        elevation = 2.dp,
        modifier = Modifier
            .padding(6.dp)
            .fillMaxWidth()
            .wrapContentHeight()
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.Red)
                .padding(8.dp)
        ) {
            Text(
                color = Color.White,
                text = message,
                fontSize = 16.sp,
                modifier = Modifier
                    .padding(start = 12.dp)
                    .align(CenterVertically)
            )

            Button(
                modifier = Modifier
                    .background(Color.Black)
                    .padding(8.dp),
                onClick = onRetryClicked) {
                Text(text = "Retry")
            }
        }
    }
}