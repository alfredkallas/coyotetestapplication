package com.ak.coyotetestapplication.ui.utils

import java.text.SimpleDateFormat
import java.util.Locale

fun String.formatDateFromISOInstantToStandard() : String {

    val parser =  SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss.SSS'Z'", Locale.getDefault())
    val formatter = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
    val parsed = parser.parse(this)
    if (parsed != null) {
        return formatter.format(parsed)
    }
    return "N/A"
}