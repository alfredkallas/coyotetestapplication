package com.ak.coyotetestapplication.ui

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ak.coyotetestapplication.domain.RandomUsersRepository
import com.ak.coyotetestapplication.model.domain.UserDetails
import com.ak.coyotetestapplication.ui.utils.UserDetailsDestination
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserDetailsViewModel @Inject constructor(private val repository: RandomUsersRepository, private val savedStateHandle: SavedStateHandle) : ViewModel() {

    private val _uiState : MutableStateFlow<UserDetailsState> = MutableStateFlow(UserDetailsState.Loading)
    var uiState : StateFlow<UserDetailsState> = _uiState

    init {
        val userId = savedStateHandle.get<Long>(UserDetailsDestination.userIdNameArgs)

        userId?.let {
            viewModelScope.launch {
                val userDetails = repository.getUserDetails(userId)
                _uiState.value = UserDetailsState.Success(userDetails)
            }
        }
    }
}

