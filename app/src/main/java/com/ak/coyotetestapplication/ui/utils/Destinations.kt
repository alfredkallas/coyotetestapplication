package com.ak.coyotetestapplication.ui.utils

import androidx.navigation.NavType
import androidx.navigation.navArgument

/**
 * This interface is used to define routes for navigation compose.
 */


interface Destination {
    /**
     * Route this is the name of the rout in the destination
     */
    val route: String
}

/**
 * This class represents the movie list Destination.
 */
object UsersListDestination : Destination {
    override val route = "usersList"
}

/**
 * This class represents the movie Details Destination and it takes arguments such as movie_details and movie_name to display information.
 */
object UserDetailsDestination : Destination {
    override val route = "userDetails"
    const val userIdNameArgs = "user_id"
    val routeWithArgs = "${route}/{${userIdNameArgs}}"
    val arguments = listOf(
        navArgument(userIdNameArgs) { type = NavType.LongType },
    )
}