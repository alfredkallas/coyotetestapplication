package com.ak.coyotetestapplication.ui

import com.ak.coyotetestapplication.model.domain.UserDetails

sealed class UserDetailsState {
    object Loading : UserDetailsState()
    data class Success(val data: UserDetails): UserDetailsState()
}
