package com.ak.coyotetestapplication.ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.ak.coyotetestapplication.model.domain.UserDetails
import com.ak.coyotetestapplication.ui.utils.formatDateFromISOInstantToStandard

/**
 * This composable represents the movie details screen in the application
 */
@Composable
fun UserDetailsScreen(
    modifier: Modifier = Modifier,
    viewModel: UserDetailsViewModel = hiltViewModel(),
) {

    val userDetailsState by viewModel.uiState.collectAsState()

    when (userDetailsState) {
        is UserDetailsState.Loading -> {
            CircularProgressIndicator(
                modifier = Modifier.fillMaxWidth()
                    .wrapContentWidth(Alignment.CenterHorizontally)
            )
        }
        is UserDetailsState.Success -> {
            UserDetailsSuccessScreen(userDetails = (userDetailsState as UserDetailsState.Success).data)
        }
    }
    // Implement composable here

}

@Composable
fun UserDetailsSuccessScreen(
    modifier: Modifier = Modifier,
    userDetails: UserDetails
) {
    Surface (
        shape = MaterialTheme.shapes.small,
        modifier = modifier
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.verticalScroll(rememberScrollState())
                .padding(vertical = 16.dp)
        ) {
            Text(
                text = "${userDetails.title} ${userDetails.firstName} ${userDetails.lastName}",
                style = MaterialTheme.typography.h3
            )
            Spacer(modifier = Modifier.width(16.dp))
            UserInformation(
                title = "Gender",
                information = userDetails.gender
            )
            Spacer(modifier = Modifier.width(16.dp))
            UserInformation(
                title = "Email",
                information = userDetails.email
            )
            Spacer(modifier = Modifier.width(16.dp))
            UserInformation(
                title = "Date of birth",
                information = userDetails.dob?.formatDateFromISOInstantToStandard()
            )
            Spacer(modifier = Modifier.width(16.dp))
            UserInformation(
                title = "Age",
                information = "${userDetails.age}"
            )
            Spacer(modifier = Modifier.width(16.dp))
            UserInformation(
                title = "Phone",
                information = userDetails.phone
            )
            Spacer(modifier = Modifier.width(16.dp))
            UserInformation(
                title = "Cell",
                information = userDetails.phone
            )
            Spacer(modifier = Modifier.width(16.dp))
            UserInformation(
                title = "Registered",
                information = getRegisteredYearsText(userDetails.registered?.age)
            )
            Spacer(modifier = Modifier.width(16.dp))
            UserInformation(
                title = "Nationality",
                information = userDetails.nat
            )
            }
        }
    }

fun getRegisteredYearsText(age:Int?) =
    if (age != null)
        "$age ${if (age > 1) "years" else "year"} ago"
    else
        "N/A"

@Composable
private fun UserInformation(title:String, information:String?) {
    Row {
        Text(
            text = "$title : "
        )
        Text(text = information ?: "N/A")
    }
}