package com.ak.coyotetestapplication.ui

import androidx.lifecycle.ViewModel
import androidx.paging.PagingData
import androidx.paging.insertSeparators
import androidx.paging.map
import com.ak.coyotetestapplication.domain.RandomUsersRepository
import com.ak.coyotetestapplication.model.ui.UiModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class UsersListViewModel @Inject constructor(private val repository: RandomUsersRepository) : ViewModel() {

    val pagingDataFlow: Flow<PagingData<UiModel>> = repository.getRandomUsers()
        .map { pagingData -> pagingData.map { UiModel.UserItem(it) } }
        .map { it ->
            it.insertSeparators { before, after ->
                if (after == null) {
                    return@insertSeparators null
                }

                if (before == null) {
                    return@insertSeparators UiModel.SeparatorItem("A")
                }

                if (before.user.firstName?.first() != after.user.firstName?.first()) {
                    after.user.firstName?.let { lastName ->
                        UiModel.SeparatorItem(lastName.first().uppercase())
                    }
                } else {
                    null
                }
            }
        }

}