package com.ak.coyotetestapplication.data.retrofit

import com.ak.coyotetestapplication.model.api.RandomUserAPI
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Random user api service to communicate using [Retrofit] with the api.
 */
interface RandomUserApiService {

    /**
     * Get users, gets the users from the api, adding a static seed to the call, guarantees that the same users are always fetched
     *
     * @param page
     * @param itemsPerPage
     * @return the results of the api call
     */
    @GET("api/?seed=abc")
    suspend fun getUsers(
        @Query("page") page: Int,
        @Query("results") itemsPerPage: Int
    ) : RandomUserAPI
}