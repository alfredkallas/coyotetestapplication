package com.ak.coyotetestapplication.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ak.coyotetestapplication.model.db.*

@Database(
    entities = [
        UserEntity::class,
        LocationEntity::class,
        RegisteredEntity::class,
        UserPictureEntity::class,
        StreetEntity::class,
        CoordinatesEntity::class,
        TimeZoneEntity::class,
        RemoteKeyEntity::class],
    version = 1,
    exportSchema = false
)
abstract class RandomUsersDatabase : RoomDatabase(){

    abstract fun usersDao(): UserDao

    abstract fun remoteKeysDao(): RemoteKeysDao

}