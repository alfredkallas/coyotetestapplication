package com.ak.coyotetestapplication.data.db

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.ak.coyotetestapplication.model.db.UserEntity

@Dao
interface UserDao {

    @Insert(onConflict = REPLACE)
    suspend fun insertAll(repos : List<UserEntity>) : List<Long>

    @Query("SELECT * FROM users " +
            "ORDER BY firstName ASC")
    fun getUsers(): PagingSource<Int, UserEntity>

    @Query("SELECT * FROM users " +
            "WHERE userId = :userId")
    fun getUserDetails(userId: Long): UserEntity

    @Query("DELETE FROM users")
    suspend fun clearUsers() : Unit
}