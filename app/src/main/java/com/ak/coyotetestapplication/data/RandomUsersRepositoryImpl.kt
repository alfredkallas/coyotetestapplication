package com.ak.coyotetestapplication.data

import androidx.paging.*
import com.ak.coyotetestapplication.data.retrofit.RandomUserApiService
import com.ak.coyotetestapplication.domain.RandomUsersRepository
import com.ak.coyotetestapplication.model.db.toUserDetailsModel
import com.ak.coyotetestapplication.model.db.toUserUIModel
import com.ak.coyotetestapplication.model.domain.User
import com.ak.coyotetestapplication.model.domain.UserDetails
import com.ak.traxtestapplication.di.IoDispatcher
import com.ak.coyotetestapplication.data.db.RandomUsersDatabase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RandomUsersRepositoryImpl @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val service: RandomUserApiService,
    private val database : RandomUsersDatabase
) : RandomUsersRepository {

    override fun getRandomUsers(): Flow<PagingData<User>> {

        val pagingSourceFactory = {
            var pagingSource = database.usersDao().getUsers()
            print(pagingSource)
            pagingSource
        }
        @OptIn(ExperimentalPagingApi::class)
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            remoteMediator = RandomUsersRemoteMediator(
                service = service,
                database = database
            ),
            pagingSourceFactory = pagingSourceFactory
        ).flow
            .map { it.map { user -> user.toUserUIModel() } }
            .flowOn(dispatcher)
    }

    override suspend fun getUserDetails(userId: Long): UserDetails {
        return withContext(dispatcher) {
            database.usersDao().getUserDetails(userId)
                .toUserDetailsModel()
        }
    }

    companion object {
        const val NETWORK_PAGE_SIZE = 30
    }

}