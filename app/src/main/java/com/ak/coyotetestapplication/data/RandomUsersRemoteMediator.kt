package com.ak.coyotetestapplication.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.ak.coyotetestapplication.data.retrofit.RandomUserApiService
import com.ak.coyotetestapplication.model.api.toUserEntity
import com.ak.coyotetestapplication.model.db.RemoteKeyEntity
import com.ak.coyotetestapplication.model.db.UserEntity
import com.ak.coyotetestapplication.data.db.RandomUsersDatabase
import okio.IOException
import retrofit2.HttpException

private const val RANDOM_USER_STARTING_PAGE_INDEX = 1

@OptIn(ExperimentalPagingApi::class)
class RandomUsersRemoteMediator(
    private val service: RandomUserApiService,
    private val database: RandomUsersDatabase
) : RemoteMediator<Int, UserEntity>() {

    override suspend fun load(loadType: LoadType, state: PagingState<Int, UserEntity>): MediatorResult {
        val page: Int = when(loadType) {
            LoadType.REFRESH -> {
                RANDOM_USER_STARTING_PAGE_INDEX
            }
            LoadType.PREPEND -> {
                // If remoteKeys is null, that means the refresh result is not in the database yet.
                return MediatorResult.Success(endOfPaginationReached = false)
            }
            LoadType.APPEND -> {
                val remoteKeys = getRemoteKeyFromLastItem(state)
                // If remoteKeys is null, that means the refresh result is not in the database yet.
                // We can return Success with endOfPaginationReached = false because Paging
                // will call this method again if RemoteKeys becomes non-null.
                // If remoteKeys is NOT NULL but its nextKey is null, that means we've reached
                // the end of pagination for append.
                val nextKey = remoteKeys?.nextKey
                    ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                nextKey
            }
        }


        return try {
            val apiResponse = service.getUsers(page, state.config.pageSize)

            val users = apiResponse.results
            val endOfPaginationReached = users.size < state.config.pageSize

            database.withTransaction {
                //clear all tables in the database
                if (loadType == LoadType.REFRESH) {
                    database.remoteKeysDao().clearRemoteKeys()
                    database.usersDao().clearUsers()
                }

                val prevKey = if (page == RANDOM_USER_STARTING_PAGE_INDEX) null else page - 1
                val nextKey = if (endOfPaginationReached) null else page + 1

                val userIds = database.usersDao().insertAll(users.map { it.toUserEntity() })

                val keys = userIds.map {
                    RemoteKeyEntity(userId = it, prevKey = prevKey, nextKey = nextKey)
                }
                database.remoteKeysDao().insertAll(keys)
            }
            MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception : IOException) {
            MediatorResult.Error(exception)
        } catch (exception : HttpException) {
            MediatorResult.Error(exception)
        }
    }

    private suspend fun getRemoteKeyFromLastItem(state: PagingState<Int, UserEntity>) : RemoteKeyEntity? {
        // Get the last page that was retrieved, that contained items.
        // From that last page, get the last item
        return state.pages.lastOrNull() { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { user ->
                // Get the remote keys of the last item retrieved
                database.remoteKeysDao().remoteKeysUserId(user.userId)
            }
    }

    private suspend fun getRemoteKeyFromFirstItem(state: PagingState<Int, UserEntity>) : RemoteKeyEntity? {
        // Get the last page that was retrieved, that contained items.
        // From that last page, get the last item
        return state.pages.firstOrNull() { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { user ->
                // Get the remote keys of the last item retrieved
                database.remoteKeysDao().remoteKeysUserId(user.userId)
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, UserEntity>
    ): RemoteKeyEntity? {
        // The paging library is trying to load data after the anchor position
        // Get the item closest to the anchor position
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.userId?.let { userId ->
                database.remoteKeysDao().remoteKeysUserId(userId)
            }
        }
    }

}