package com.ak.coyotetestapplication.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ak.coyotetestapplication.model.domain.TimeZone

/**
 * Timezone represented the Timezone for the location of the user fetched from the api.
 *
 * @property offset represents the timezone offset from GMT.
 * @property description represents the description of the timezone, e.g Brussels, Copenhagen, Madrid, Paris"
 * @constructor Create an instance of TimeZone in db.
 */
@Entity(tableName = "timezone")
data class TimeZoneEntity(
    val offset: String?,
    val description: String?
    ) {
    @PrimaryKey(autoGenerate = true) var timezoneId: Int = 0
}

/**
 * To TimeZone UI Model
 * converts the TimeZone DB Model to TimeZone UI Model.
 */
fun TimeZoneEntity.toTimZoneUIModel() =
    TimeZone(
        offset = offset,
        description = description
    )
