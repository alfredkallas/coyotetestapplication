package com.ak.coyotetestapplication.model.domain

/**
 * User picture represents the picture information of the user.
 *
 * @property large represents the large scale picture of the user.
 * @property medium represents the medium scale picture of the user.
 * @constructor Create an instance of User picture in db.
 */
data class UserPicture(
    val large: String?,
    var largeImagePath: String? = null,
    val medium: String?,
    var mediumImagePath: String? = null,
)
