package com.ak.coyotetestapplication.model.api

/**
 * Random user api represents the returned result from the api.
 *
 * @property results represents the results of the api call.
 * @constructor Create an instance of Random user api
 */
data class RandomUserAPI(
    val results: List<UserAPIModel>,
    val info: InfoAPIModel
)