package com.ak.coyotetestapplication.model.api

import com.ak.coyotetestapplication.model.db.StreetEntity

/**
 * Street represented the Street for the user's location fetched from the api.
 *
 * @property number represents the street number.
 * @property name represents the street name.
 * @constructor Create a Street instance.
 */
data class StreetAPIModel(
    val number: Int?,
    val name: String?
)

/**
 * To street Entity
 * converts the street api model to street db model.
 */
fun StreetAPIModel.toStreetEntity() =
    StreetEntity(
        number = number,
        name = name
    )