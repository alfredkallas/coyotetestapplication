package com.ak.coyotetestapplication.model.api

import com.ak.coyotetestapplication.model.db.CoordinatesEntity

/**
 * Coordinates represents the exact coordinates of the user's location fetched from the api.
 *
 * @property latitude represents the latitude of the user's location.
 * @property longitude represents the longitude of the user's location.
 * @constructor Create an instance of Coordinates.
 */
data class CoordinatesAPIModel(
    val latitude: Double?,
    val longitude: Double?
)

/**
 * To coordinates DB model
 * converts the coordinates API Model to coordinates DB Model.
 */
fun CoordinatesAPIModel.toCoordinatesEntity() =
    CoordinatesEntity(
        latitude = latitude,
        longitude = longitude
    )