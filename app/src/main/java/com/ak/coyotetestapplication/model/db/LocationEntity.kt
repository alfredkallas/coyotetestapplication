package com.ak.coyotetestapplication.model.db

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ak.coyotetestapplication.model.domain.Location

/**
 * Location, this class represents the location of the user from the api.
 *
 * @property street, this represents the information of the street using [Street].
 * @property city this represents the name of the city.
 * @property state this represents the name of the state.
 * @property country this represents the name of the country.
 * @property postcode this represents the postcode of the user.
 * @property coordinates this represents the exact coordinates of the user using [Coordinates].
 * @property timeZone this represents the Timezone of the location of the user using [TimeZone].
 * @constructor Create a Location instance in db.
 */
@Entity(tableName = "location")
data class LocationEntity(
    @Embedded val street: StreetEntity,
    val city: String,
    val state: String,
    val country: String,
    val postcode: String,
    @Embedded val coordinates: CoordinatesEntity,
    @Embedded val timeZone: TimeZoneEntity
) {
    @PrimaryKey(autoGenerate = true) var locationId: Int = 0
}

/**
 * To location UI model
 * converts the location DB Model to location UI Model.
 */
fun LocationEntity.toLocationUIModel() = Location(
    street = street.toStreetUIModel(),
    city = city,
    state = state,
    country = country,
    postcode = postcode,
    coordinates = coordinates.toCoordinatesUIModel(),
    timeZone = timeZone.toTimZoneUIModel()
)