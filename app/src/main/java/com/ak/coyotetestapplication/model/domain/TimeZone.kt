package com.ak.coyotetestapplication.model.domain

/**
 * Timezone represented the Timezone for the location of the user fetched from the api.
 *
 * @property offset represents the timezone offset from GMT.
 * @property description represents the description of the timezone, e.g Brussels, Copenhagen, Madrid, Paris"
 * @constructor Create an instance of TimeZone in db.
 */
data class TimeZone(
    val offset: String?,
    val description: String?
    )