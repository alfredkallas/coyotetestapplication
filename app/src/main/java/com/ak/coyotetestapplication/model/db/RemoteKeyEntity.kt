package com.ak.coyotetestapplication.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Remote key database model to save latest page number information.
 *
 * @property userId a single id of remote key element.
 * @property prevKey the previous key page for user.
 * @property nextKey the next key page for user.
 * @constructor Create a remote key model in db.
 */
@Entity(tableName = "remote_keys")
data class RemoteKeyEntity(
    @PrimaryKey
    val userId: Long,
    val prevKey: Int?,
    val nextKey: Int?
)