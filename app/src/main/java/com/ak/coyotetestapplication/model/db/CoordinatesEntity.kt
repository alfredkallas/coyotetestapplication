package com.ak.coyotetestapplication.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ak.coyotetestapplication.model.domain.Coordinates

/**
 * Coordinates represents the exact coordinates of the user's location fetched from the api.
 *
 * @property latitude represents the latitude of the user's location.
 * @property longitude represents the longitude of the user's location.
 * @constructor Create an instance of Coordinates in db.
 */
@Entity(tableName = "coordinates")
data class CoordinatesEntity(
    val latitude: Double?,
    val longitude: Double?
) {
    @PrimaryKey(autoGenerate = true) var coordinatesId: Int = 0
}

/**
 * To coordinates UI model
 * converts the coordinates DB Model to coordinates UI Model.
 */
fun CoordinatesEntity.toCoordinatesUIModel() =
    Coordinates(
        latitude = latitude,
        longitude = longitude
    )