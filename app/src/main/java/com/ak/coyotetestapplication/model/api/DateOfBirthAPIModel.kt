package com.ak.coyotetestapplication.model.api

/**
 * Date of birth represent the date of birth of the user fetched from the api.
 *
 * @property date represents the user's [String] formatted date of birth.
 * @property age represents the age of the user in [Int].
 * @constructor Create an instance of Date of birth.
 */
data class DateOfBirthAPIModel(
    val date: String?,
    val age : Int?
)
