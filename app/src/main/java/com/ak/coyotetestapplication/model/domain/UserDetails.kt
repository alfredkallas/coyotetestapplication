package com.ak.coyotetestapplication.model.domain

/**
 * User represents the user information fetched from the api.
 *
 * @property location represents the location information of the user using [Location].
 * @property email represents the email in [String] of the user.
 * @property dob represents the date of birth information of the user using [String].
 * @property registered represents the registration information of the user using [Registered].
 * @property phone represents the phone number of the user in [String].
 * @property cell represents the cell phone number of the user in [String].
 * @property picture represents the profile pictures information of the user using [UserPicture].
 * @property nat represents the nationality of the user in [String].
 * @constructor Create an instance of User in db.
 */

data class UserDetails(
    val gender: String?,
    val title: String?,
    val firstName: String?,
    val lastName: String?,
    val location: Location?,
    val email: String?,
    val dob: String?,
    val age: Int?,
    val registered: Registered?,
    val phone: String?,
    val cell: String?,
    val picture: UserPicture?,
    val nat: String?
)