package com.ak.coyotetestapplication.model.domain

/**
 * User represents the user information fetched from the api.
 *
 * @property gender represents the gender of the user.
 * @property title represents the title of the user.
 * @property firstName represents the first Name of the user.
 * @property lastName represents the last name of the user.
 * @property thumbnail represents the image thumbnail network location for the user.
 * @property thumbnailImagePath represents the image thumbnail local storage path location for the user.
 * @constructor Create an instance of User for use in ui.
 */

data class User(
    val userId: Long,
    val gender: String?,
    val title: String?,
    val firstName: String?,
    val lastName: String?,
    val thumbnail: String?,
    var thumbnailImagePath: String?
)