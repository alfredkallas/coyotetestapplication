package com.ak.coyotetestapplication.model.ui

import com.ak.coyotetestapplication.model.domain.User

sealed class UiModel {
    data class UserItem(val user : User) : UiModel()
    data class SeparatorItem(val description : String) : UiModel()
}