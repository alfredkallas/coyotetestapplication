package com.ak.coyotetestapplication.model.db

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ak.coyotetestapplication.model.domain.User
import com.ak.coyotetestapplication.model.domain.UserDetails

/**
 * User represents the user information fetched from the api.
 *
 * @property gender represents the gender of the user.
 * @property gender represents the gender of the user.
 * @property title represents the title of the user.
 * @property firstName represents the first Name of the user.
 * @property lastName represents the last name of the user.
 * @property thumbnail represents the image thumbnail network location for the user.
 * @property thumbnailImagePath represents the image thumbnail local storage path location for the user.
 * @constructor Create an instance of User in db.
 */

@Entity(tableName = "users")
data class UserEntity(
    val gender: String?,
    val title: String?,
    val firstName: String?,
    val lastName: String?,
    @Embedded val location: LocationEntity?,
    val email: String?,
    val dob: String?,
    val age: Int?,
    @Embedded
    val registered: RegisteredEntity?,
    val phone: String?,
    val cell: String?,
    @Embedded
    val picture: UserPictureEntity?,
    val nat: String?
) {
    @PrimaryKey(autoGenerate = true) var userId: Long = 0
}

/**
 * To user entity
 * converts the User DB Model to User UI Model.
 */
fun UserEntity.toUserUIModel() = User(
    userId = userId,
    gender = gender,
    title = title,
    firstName = firstName,
    lastName = lastName,
    thumbnail = picture?.thumbnail,
    thumbnailImagePath = picture?.thumbnailImagePath
)

/**
 * To user Details
 * converts the User DB Model to User Details UI Model.
 */
fun UserEntity.toUserDetailsModel() = UserDetails(
    gender = gender,
    title = title,
    firstName = firstName,
    lastName = lastName,
    location = location?.toLocationUIModel(),
    email = email,
    dob = dob,
    age = age,
    registered = registered?.toRegisteredUIModel(),
    phone = phone,
    cell = cell,
    picture = picture?.toUserPictureUIModel(),
    nat = nat
)