package com.ak.coyotetestapplication.model.api

import androidx.room.PrimaryKey
import com.ak.coyotetestapplication.model.db.LocationEntity

/**
 * Location, this class represents the location of the user from the api.
 *
 * @property street, this represents the information of the street using [StreetAPIModel].
 * @property city this represents the name of the city.
 * @property state this represents the name of the state.
 * @property country this represents the name of the country.
 * @property postcode this represents the postcode of the user.
 * @property coordinates this represents the exact coordinates of the user using [CoordinatesAPIModel].
 * @property timeZone this represents the Timezone of the location of the user using [TimeZoneAPIModel].
 * @constructor Create a Location instance.
 */
data class LocationAPIModel(
    val street: StreetAPIModel,
    val city: String,
    val state: String,
    val country: String,
    val postcode: String,
    val coordinates: CoordinatesAPIModel,
    val timezone: TimeZoneAPIModel
)

/**
 * To location DB model
 * converts the location API Model to location DB Model.
 */
fun LocationAPIModel.toLocationEntity() = LocationEntity(
    street = street.toStreetEntity(),
    city = city,
    state = state,
    country = country,
    postcode = postcode,
    coordinates = coordinates.toCoordinatesEntity(),
    timeZone = timezone.toTimZoneEntity()
)