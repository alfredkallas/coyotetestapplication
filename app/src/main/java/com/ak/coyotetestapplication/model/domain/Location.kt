package com.ak.coyotetestapplication.model.domain

/**
 * Location, this class represents the location of the user from the api.
 *
 * @property street, this represents the information of the street using [Street].
 * @property city this represents the name of the city.
 * @property state this represents the name of the state.
 * @property country this represents the name of the country.
 * @property postcode this represents the postcode of the user.
 * @property coordinates this represents the exact coordinates of the user using [Coordinates].
 * @property timeZone this represents the Timezone of the location of the user using [TimeZone].
 * @constructor Create a Location instance in db.
 */
data class Location(
    val street: Street?,
    val city: String?,
    val state: String?,
    val country: String?,
    val postcode: String?,
    val coordinates: Coordinates?,
    val timeZone: TimeZone?
)