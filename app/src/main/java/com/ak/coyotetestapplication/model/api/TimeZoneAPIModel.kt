package com.ak.coyotetestapplication.model.api

import com.ak.coyotetestapplication.model.db.TimeZoneEntity

/**
 * Timezone represented the Timezone for the location of the user fetched from the api.
 *
 * @property offset represents the timezone offset from GMT.
 * @property description represents the description of the timezone, e.g Brussels, Copenhagen, Madrid, Paris"
 * @constructor Create an instance of TimeZone.
 */
data class TimeZoneAPIModel(
    val offset: String?,
    val description: String?
    )

/**
 * To TimeZone DB model
 * converts the TimeZone API Model to TimeZone DB Model.
 */
fun TimeZoneAPIModel.toTimZoneEntity() =
    TimeZoneEntity(
        offset = offset,
        description = description
    )
