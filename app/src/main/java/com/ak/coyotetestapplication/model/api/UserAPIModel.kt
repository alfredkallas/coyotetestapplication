package com.ak.coyotetestapplication.model.api

import androidx.room.PrimaryKey
import com.ak.coyotetestapplication.model.db.UserEntity

/**
 * User represents the user information fetched from the api.
 *
 * @property userId represents the id of the user.
 * @property gender represents the gender of the user.
 * @property name represents the name information of the user using [NameAPIModel].
 * @property location represents the location information of the user using [LocationAPIModel].
 * @property email represents the email in [String] of the user.
 * @property login represents the login information of the user using [LoginAPIModel].
 * @property dob represents the date of birth information of the user using [DateOfBirthAPIModel].
 * @property registered represents the registration information of the user using [RegisteredAPIModel].
 * @property phone represents the phone number of the user in [String].
 * @property cell represents the cell phone number of the user in [String].
 * @property id represents the id information of the user using [UserIdAPIModel].
 * @property picture represents the profile pictures information of the user using [UserPictureAPIModel].
 * @property nat represents the nationality of the user in [String].
 * @constructor Create an instance of User.
 */
data class UserAPIModel(
    val gender: String?,
    val name: NameAPIModel?,
    val location: LocationAPIModel?,
    val email: String?,
    val login: LoginAPIModel?,
    val dob: DateOfBirthAPIModel?,
    val registered: RegisteredAPIModel?,
    val phone: String?,
    val cell: String?,
    val id: UserIdAPIModel?,
    val picture: UserPictureAPIModel?,
    val nat: String?
)

/**
 * To user entity
 * converts the User API Model to User DB Model.
 */
fun UserAPIModel.toUserEntity() = UserEntity(
    gender = gender,
    title = name?.title,
    firstName = name?.first,
    lastName = name?.last,
    location = location?.toLocationEntity(),
    email = email,
    dob = dob?.date,
    age = dob?.age,
    registered = registered?.toRegisteredEntity(),
    phone = phone,
    cell = cell,
    picture = picture?.toUserPictureEntity(),
    nat = nat
)