package com.ak.coyotetestapplication.model.domain

/**
 * Street represented the Street for the user's location fetched from the api.
 *
 * @property number represents the street number.
 * @property name represents the street name.
 * @constructor Create a Street instance in db.
 */
data class Street(
    val number: Int?,
    val name: String?
)