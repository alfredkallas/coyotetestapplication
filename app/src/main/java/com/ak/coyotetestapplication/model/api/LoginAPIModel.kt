package com.ak.coyotetestapplication.model.api

import androidx.room.PrimaryKey

/**
 * Login represents the login information for the user from the api.
 *
 * @property uui represents the uuid of the user.
 * @property username represents the username of the user.
 * @property password represents the password of the user.
 * @property salt represents the salt of the user.
 * @property md5 represents the md5 of the user.
 * @property sha1 represents the sha1 of the user.
 * @property sha256 represents the sha256 of the user.
 * @constructor Create an instance of Login.
 */
data class LoginAPIModel(
    @PrimaryKey val uui: String,
    val username: String?,
    val password: String?,
    val salt: String?,
    val md5: String?,
    val sha1: String?,
    val sha256: String?
)
