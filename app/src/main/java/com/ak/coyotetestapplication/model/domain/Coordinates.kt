package com.ak.coyotetestapplication.model.domain

/**
 * Coordinates represents the exact coordinates of the user's location fetched from the api.
 *
 * @property latitude represents the latitude of the user's location.
 * @property longitude represents the longitude of the user's location.
 * @constructor Create an instance of Coordinates in db.
 */
data class Coordinates(
    val latitude: Double?,
    val longitude: Double?
)