package com.ak.coyotetestapplication.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ak.coyotetestapplication.model.domain.Registered

/**
 * Registered represents the registration date information of the user fetched from the api.
 *
 * @property date represents the date in formatted [String] for the registration of the user.
 * @property age represents the number in years since the user is registered.
 * @constructor Create an instance of Registered in db.
 */
@Entity(tableName = "registered")
data class RegisteredEntity(
    val date: String?,
    val registeredAge: Int?
) {
    @PrimaryKey(autoGenerate = true) var registeredId: Int = 0
}

/**
 * To Registered UI model
 * converts the Registered DB Model to Registered UI Model.
 */
fun RegisteredEntity.toRegisteredUIModel() = Registered (
    date = date,
    age = registeredAge
)