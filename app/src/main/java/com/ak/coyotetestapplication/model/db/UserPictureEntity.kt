package com.ak.coyotetestapplication.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ak.coyotetestapplication.model.domain.UserPicture

/**
 * User picture represents the picture information of the user.
 *
 * @property large represents the large scale picture of the user.
 * @property medium represents the medium scale picture of the user.
 * @constructor Create an instance of User picture in db.
 */
@Entity(tableName = "user_picture")
data class UserPictureEntity(
    val large: String?,
    var largeImagePath: String? = null,
    val medium: String?,
    var mediumImagePath: String? = null,
    val thumbnail: String?,
    var thumbnailImagePath: String? = null,
) {
    @PrimaryKey(autoGenerate = true) var userPictureId: Int = 0
}

/**
 * To UserPicture UI model
 * converts the UserPicture DB Model to UserPicture UI Model.
 */
fun UserPictureEntity.toUserPictureUIModel() = UserPicture(
    large = large,
    medium = medium
)
