package com.ak.coyotetestapplication.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ak.coyotetestapplication.model.domain.Street

/**
 * Street represented the Street for the user's location fetched from the api.
 *
 * @property number represents the street number.
 * @property name represents the street name.
 * @constructor Create a Street instance in db.
 */
@Entity(tableName = "street")
data class StreetEntity(
    val number: Int?,
    val name: String?
) {
    @PrimaryKey(autoGenerate = true) var streetId: Int = 0
}

/**
 * To Street UI Model
 * converts the Street DB Model to Street UI Model.
 */
fun StreetEntity.toStreetUIModel() = Street(
    number = number,
    name = name
)