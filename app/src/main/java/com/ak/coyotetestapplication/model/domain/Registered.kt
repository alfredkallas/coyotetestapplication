package com.ak.coyotetestapplication.model.domain

/**
 * Registered represents the registration date information of the user fetched from the api.
 *
 * @property date represents the date in formatted [String] for the registration of the user.
 * @property age represents the number in years since the user is registered.
 * @constructor Create an instance of Registered in db.
 */
data class Registered(
    val date: String?,
    val age: Int?
)
