package com.ak.coyotetestapplication.model.api

/**
 * User id represents the id of the user fetched from the api.
 *
 * @property name is the id representation of the user.
 * @property value represents the value of the id.
 * @constructor Create an instance of User id.
 */
data class UserIdAPIModel(
    val name: String,
    val value: String?
)
