package com.ak.coyotetestapplication.model.api

/**
 * Name represented the Name of the user from the api.
 *
 * @property title represents the title of the user.
 * @property first represents the first name of the user.
 * @property last represents the last name of the user.
 * @constructor Create an instance of Name.
 */
data class NameAPIModel(
    val title: String?,
    val first: String?,
    val last: String?
)