package com.ak.coyotetestapplication.model.api

/**
 * Info represents the information about the pagination of the api.
 *
 * @property seed represents the same string from the same generation of users.
 * @property results represents the number of users returned in the api.
 * @property page represents the number of page of the api.
 * @property version represents the version of the api.
 * @constructor Create an instance of Info.
 */
data class InfoAPIModel(
    val seed: String,
    val results: Int,
    val page: Int,
    val version: String
)