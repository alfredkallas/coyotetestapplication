package com.ak.coyotetestapplication.model.api

import com.ak.coyotetestapplication.model.db.RegisteredEntity

/**
 * Registered represents the registration date information of the user fetched from the api.
 *
 * @property date represents the date in formatted [String] for the registration of the user.
 * @property age represents the number in years since the user is registered.
 * @constructor Create an instance of Registered,
 */
data class RegisteredAPIModel(
    val date: String?,
    val age: Int?
)

/**
 * To Registered DB model
 * converts the Registered API Model to Registered DB Model.
 */
fun RegisteredAPIModel.toRegisteredEntity() = RegisteredEntity (
    date = date,
    registeredAge = age
        )