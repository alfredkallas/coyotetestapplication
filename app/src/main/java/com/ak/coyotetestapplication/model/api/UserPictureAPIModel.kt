package com.ak.coyotetestapplication.model.api

import com.ak.coyotetestapplication.model.db.UserPictureEntity

/**
 * User picture represents the picture information of the user.
 *
 * @property large represents the large scale picture of the user.
 * @property medium represents the medium scale picture of the user.
 * @property thumbnail represents the thumbnail scale picture of the user.
 * @constructor Create an instance of User picture.
 */
data class UserPictureAPIModel(
    val large: String?,
    val medium: String?,
    val thumbnail: String?
)

/**
 * To UserPicture DB model
 * converts the UserPicture API Model to UserPicture DB Model.
 */
fun UserPictureAPIModel.toUserPictureEntity() = UserPictureEntity(
    large = large,
    medium = medium,
    thumbnail = thumbnail
)
