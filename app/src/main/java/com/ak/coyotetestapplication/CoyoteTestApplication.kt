package com.ak.coyotetestapplication

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CoyoteTestApplication : Application()