package com.ak.coyotetestapplication.di

import com.ak.coyotetestapplication.data.RandomUsersRepositoryImpl
import com.ak.coyotetestapplication.domain.RandomUsersRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class RepositoryModule {
    /**
     * This function provides the [RandomUsersRepository] interface using the [RandomUsersRepositoryImpl].
     */
    @Binds
    abstract fun provideMoviesRepository(moviesRepositoryImpl: RandomUsersRepositoryImpl): RandomUsersRepository
}