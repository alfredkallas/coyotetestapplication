package com.ak.traxtestapplication.di

import javax.inject.Qualifier

/**
 * These qualifiers represents the different dispatchers used and that can be injected in the application.
 */
@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class DefaultDispatcher

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class IoDispatcher

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class MainDispatcher