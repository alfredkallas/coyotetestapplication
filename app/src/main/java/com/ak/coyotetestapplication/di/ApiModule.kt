package com.ak.coyotetestapplication.di

import com.ak.coyotetestapplication.data.retrofit.RandomUserApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * This Module represents the injection of retrofit in the application using hilt
 */
@Module
@InstallIn(SingletonComponent::class)
object ApiModule {
    private const val BASE_URL = "https://randomuser.me/"

    /**
     * This function provides a singleton instance for the http logging interceptor
     * to enable logging in the retrofit library
     */
    @Singleton
    @Provides
    fun providesHttpLoggingInterceptor() = HttpLoggingInterceptor()
        .apply {
            level = HttpLoggingInterceptor.Level.BASIC
        }

    /**
     * Provides the [OkHttpClient] with the needed certificate ca to get the information form the Json.
     *
     * @param httpLoggingInterceptor represents the interceptor used to log information from the api.
     * @return an [OkHttpClient] instance.
     */
    @Singleton
    @Provides
    fun providesOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
        OkHttpClient
            .Builder()
            .addInterceptor(httpLoggingInterceptor)
            .build()

    /**
     * Provides the [Retrofit] singleton instance for the application
     *
     * @param okHttpClient represents the [OkHttpClient] provided in the function above.
     * @return an instance of [Retrofit] to be injected in the application.
     */
    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .build()

    /**
     * Provide the [RandomUserApiService] singleton instance for the application to be used in the repository
     *
     * @param retrofit represents the [Retrofit] class created in the function above, to be injected in this function.
     * @return an instance of [RandomUserApiService] to call the api.
     */
    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): RandomUserApiService = retrofit.create(
        RandomUserApiService::class.java)
}