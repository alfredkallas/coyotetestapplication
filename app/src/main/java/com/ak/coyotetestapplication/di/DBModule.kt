package com.ak.coyotetestapplication.di

import android.content.Context
import androidx.room.Room
import com.ak.coyotetestapplication.data.db.RandomUsersDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DBModule {

    @Provides
    @Singleton
    fun provideRandomUserDatabase(@ApplicationContext context: Context): RandomUsersDatabase {
        return Room.databaseBuilder(
            context,
            RandomUsersDatabase::class.java,
            "randomUsersDb"
        ).build()
    }

}