package com.ak.traxtestapplication.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

/**
 * Coroutines dispatchers module
 *
 * This object class represents the coroutine dispatcher objects used to be injected in the repository
 * in order to run by default on the dedicated dispatched i.e IoDispatcher for the api calls.
 *
 */
@InstallIn(SingletonComponent::class)
@Module
object CoroutinesDispatchersModule {

    /**
     * Provides default dispatcher for cpu computation.
     *
     * @return the Default Dispatcher
     */
    @DefaultDispatcher
    @Provides
    fun providesDefaultDispatcher(): CoroutineDispatcher = Dispatchers.Default

    /**
     * Provides io dispatcher for background threads computation e.g: network calls or database calls.
     *
     * @return the io Dispatcher
     */
    @IoDispatcher
    @Provides
    fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    /**
     * Provides Main dispatcher used for User Interface thread.
     *
     * @return the Main Dispatcher
     */
    @MainDispatcher
    @Provides
    fun providesMainDispatcher(): CoroutineDispatcher = Dispatchers.Main
}