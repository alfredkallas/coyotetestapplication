# CoyoteTestApplication

## Name
CoyoteTestApplication.

## Description
This is the application test for the Coyote Senior Android Developer position.

It is an application that fetches information from the https://randomuser.me/ api for pagination. It is developped using the Following :

- MVVM Architecture
- Paging 3.0 from Android to fetch different pages in the application.
- Pure Kotlin
- Hilt for dependency injection
- Jetpack Compose for the UI
- Room Database for caching
- Flow in order to have streams of data
- Coil Image in order to download needed images
- Retrofit with Gson for api calls

The application launches and it shows the paging list, but for some reasons it gets stuck when loading data on the second page, tried to debug it, but did not find a solution. My guess would be something related to the database with the remote mediator within the paging 3.0 library.

When clicking on an Item we are redirected to another screen to show the user's information. A Date formating is applied to the registered date so it can be shown correctly